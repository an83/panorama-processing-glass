package processing.test.a5_log;

import java.lang.reflect.Array;
import java.util.ArrayList;

import com.google.android.glass.touchpad.Gesture;
import com.google.android.glass.touchpad.GestureDetector;

import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	protected static final String TAG = "MainActivity";
	ToggleButton tglCenteredRadar, tglBox, tglContextCompass,
		tglPointing, tglDrawing, tglPointingDrawing,
		toggleSimulate;
	TextView txtIP;
	private GestureDetector mGestureDetector;
	private Button btnStart;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		tglCenteredRadar = (ToggleButton) findViewById(R.id.tglCenteredRadar);
		tglBox = (ToggleButton) findViewById(R.id.tglBox);
		tglContextCompass = (ToggleButton) findViewById(R.id.tglContextCompass);
		tglPointing = (ToggleButton) findViewById(R.id.tglPointing);
		tglDrawing = (ToggleButton) findViewById(R.id.tglDrawing);
		toggleSimulate = (ToggleButton) findViewById(R.id.toggleSimulate);
		
		btnStart = (Button) findViewById(R.id.button1);
		
		txtIP = (TextView) findViewById(R.id.txtIP);
		
		mGestureDetector = createGestureDetector(this);
	}
	private GestureDetector createGestureDetector(Context context) {
	    GestureDetector gestureDetector = new GestureDetector(context);
	        //Create a base listener for generic gestures
	        gestureDetector.setBaseListener( new GestureDetector.BaseListener() {
	            @Override
	            public boolean onGesture(Gesture gesture) {
	                if (gesture == Gesture.TAP) {
	                    // do something on tap
	                    return true;
	                } else if (gesture == Gesture.TWO_TAP) {
	                    // do something on two finger tap
	                	Log.d(TAG, "TWO TAP");
//	                	toggleSimulate.performClick();
	                	btnStart.performClick();
	                	
	                    return true;
	                } else if (gesture == Gesture.SWIPE_RIGHT) {
	                    // do something on right (forward) swipe
	                    return true;
	                } else if (gesture == Gesture.SWIPE_LEFT) {
	                    // do something on left (backwards) swipe
	                    return true;
	                }
	                return false;
	            }

	        });
	        return gestureDetector;
	    }

	    /*
	     * Send generic motion events to the gesture detector
	     */
	    @Override
	    public boolean onGenericMotionEvent(MotionEvent event) {
	        if (mGestureDetector != null) {
	            return mGestureDetector.onMotionEvent(event);
	        }
	        return false;
	    }
	
	@Override
	protected void onResume() {		
		super.onResume();
		
		WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
		String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
		
		txtIP.setText(ip);
		
//		toggleSimulate.performClick();
//    	btnStart.performClick();
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void onButtonClick(View view){
		Intent intent = new Intent(this, a5_log.class);
		
		if(tglBox.isChecked()) intent.putExtra(SCENARIO.RADAR_BOX.toString(), true);
		if(tglCenteredRadar.isChecked()) intent.putExtra(SCENARIO.CENTERED_RADAR.toString(), true);
		if(tglContextCompass.isChecked()) intent.putExtra(SCENARIO.CONTEXT_COMPASS.toString(), true);
		
		if(tglPointing.isChecked()) intent.putExtra(SCENARIO.POINTING.toString(), true);
		if(tglDrawing.isChecked()) intent.putExtra(SCENARIO.DRAWING.toString(), true);
		if(toggleSimulate.isChecked()) intent.putExtra(Extra.EXTRA_SIMULATE, true);
		
		startActivity(intent);
	}

}
