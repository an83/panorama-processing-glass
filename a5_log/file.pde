import java.io.*;
import java.io.File;
import java.lang.StringBuilder;
import java.lang.Thread;
import android.os.Handler;
import java.io.FileWriter;
import java.io.BufferedWriter;
import android.os.Environment;
import java.util.Date;
import java.text.*;

class File {  

  PrintWriter output;
  long lastUpdate = 0;
  int pauseInitial = 2000; 
  int pausePerSecond = 500; 
  Thread thread;

  File()
  {
    int D = day();    // Values from 1 - 31
    int M = month();  // Values from 1 - 12
    int Y = year();   // 2003, 2004, 2005, etc.

    int ms = millis();
    int s = second();  // Values from 0 - 59
    int m = minute();  // Values from 0 - 59
    int h = hour();    // Values from 0 - 23

    String fileName = String.format("Experiment_%s%s%s_%s%s%s_%s.csv", Y, M, D, h, m, s, ms);    
    String basePath = Environment.getExternalStorageDirectory().getAbsolutePath();
    String directory = "/panorama_experiment/";
    String filePath = basePath + directory+ fileName; 

    output = createWriter(filePath);
  }

  void readFileAsync() {     
    thread = new Thread() {
      public void run() {
        readFile();
      }
    };

    thread.start();
  }

  void readFile() {
    Boolean skip1 = true, skip2 = true;
    java.io.File sdcard = Environment.getExternalStorageDirectory();

    String startTime = "18:38";

    java.io.File file = new java.io.File(sdcard, "pano_logs/Experiment 2014_2_20_18_34_0_1093.csv");

    println("readFile start...");
    try {
      Thread.sleep(pauseInitial);

      BufferedReader br = new BufferedReader(new FileReader(file));      
      String line = br.readLine();      
      Date lastDate1 = new Date();
      Boolean nextLine = false;

      while ( line != null ) { 
        Date date1 = getTime(line);      

        if (date1 == null) {
          line = br.readLine();
        }

        if ((date1 == null && line != null)) {
          continue;
        }

        if (date1 == null) date1 = new Date();


        if (skip1 && !date1.toString().contains(startTime)) {
          println(String.format("skipping... %s", line));
          line = br.readLine();
          continue;
        }
        else {
          skip1 = false;
        }

        println(String.format("date1: %s", date1));

        lastDate1 = compareToLastDate(date1, lastDate1);
        lastDate1 = date1;

        processTabletFile(line);
        line = br.readLine();
      }
    }
    catch (IOException e) {
      //You'll need to add proper error handling here
      println("ERROR! readFile:IOException " + e.getMessage());
    }
    catch(InterruptedException e) { 
      println("ERROR! readFile:InterruptedException " + e.getMessage());
    }

    println("readFile success!");
  }

  void processTabletFile(String line) {
    println(line);
    String [] tokens = line.split(",");

    String time = tokens[1];
    String type = tokens[2];

    if (type.equals("local")) {
      println("local");

      remoteOrientation.set(float(tokens[3]), 0, 0);
//      remoteOrientation.set(float(tokens[8]), 0, 0);
    } 
    else if (type.equals("POINTING")) {
      println("POINTING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));
      
      p.scaleTo640x360();

      pointing.remotePointingPoint.set(p);
    } 
    else if (type.equals("DRAWING")) {
      println("DRAWING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));
      
      p.scaleTo640x360();

      drawing.remotePoints.add(p);
    }
    else if (type.equals("CLEAR")) {
      println("CLEAR");

      drawing.remotePoints.clear();
    }
  }

  DateFormat df = new SimpleDateFormat("yyyy-M-d H:m:s");
  DateFormat tf = new SimpleDateFormat("HH:mm:ss");

  Date getTime(String line) {
    //    println("getTime line: " + line);

    try {
      String [] tokens = line.split(",");
      //      println("tokens length: " + tokens.length);

      String time = tokens[1];
      //      println("time: " + time);

      Date date =  df.parse(time);
      //      println(date);

      return date;
    }
    catch(Exception e) {
      println(String.format("Error parsing [%s]: %s", line, e));
      return null;
    }
  }
  
  Date compareToLastDate(Date date, Date lastDate) throws InterruptedException {
    if (!date.equals(lastDate)) {
      println(String.format("sleeping... date: %s last: %s", date, lastDate));
      Thread.sleep(pausePerSecond);

      lastDate = date;
    }

    return lastDate;
  }


  boolean isReady() 
  {
    if (System.currentTimeMillis() - lastUpdate > 100) {
      lastUpdate = System.currentTimeMillis();
      return true;
    }

    return false;
  }

  void writeTime()
  {
    int D = day();    // Values from 1 - 31
    int M = month();  // Values from 1 - 12
    int Y = year();   // 2003, 2004, 2005, etc.

    int s = second();  // Values from 0 - 59
    int m = minute();  // Values from 0 - 59
    int h = hour();    // Values from 0 - 23

    output.print(String.format("GLASS,%s-%s-%s %s:%s:%s", Y, M, D, h, m, s));
  }

  void write(String text)
  {
    writeTime();
    output.print(String.format(",%s", text));
    output.println();
    output.flush();
  }

  void writeVector(PVector vector, String name)
  {
    writeTime();
    output.print(String.format(",%s,%s,%s,%s", name, vector.x, vector.y, vector.z));
  }

  void writeOrientation(PVector local, PVector remote)
  {
    if (isReady()) {
      writeVector(local, "local");
      writeVector(remote, "remote");
      output.println();
      output.flush();
    }
  }    

  void writeAnnotation(ANNOTATION_TYPE annotation, Point point) {
    writeTime();
    output.print(String.format(",%s,%s,%s,%s,%s,%s", annotation, point.a, point.b, point.x, point.y, point.o));
    output.println();
    output.flush();
  }

  void writeClearAnnotation() {
    writeTime();
    output.print(String.format(",CLEAR"));
    output.println();
    output.flush();
  }


  void destroy()
  {
    output.flush(); // Writes the remaining data to the file
    output.close(); // Finishes the file
  }
}

