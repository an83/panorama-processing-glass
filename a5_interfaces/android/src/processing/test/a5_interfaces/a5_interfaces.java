package processing.test.a5_interfaces;

import processing.core.*;
import processing.data.*;
import processing.event.*;
import processing.opengl.*;

import ketai.sensors.*;
import ketai.ui.*;
import android.view.MotionEvent;
import java.util.Arrays;
import java.lang.Enum;
import android.os.Bundle;
import oscP5.*;
import netP5.*;
import java.io.FileWriter;
import java.io.BufferedWriter;
import android.os.Environment;

import apwidgets.*;

import java.util.HashMap;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.IOException;

public class a5_interfaces extends PApplet {
	enum ANNOTATION_TYPE {
		DRAWING, POINTING
	}

	enum SCENARIO {
		AUDIO, RADAR_BOX, CONTEXT_COMPASS, CENTERED_RADAR, POINTING, DRAWING, POINTING_DRAWING
	}

	SCENARIO scenario = SCENARIO.POINTING_DRAWING;
	ANNOTATION_TYPE localAnnotation = scenario == SCENARIO.DRAWING ? ANNOTATION_TYPE.DRAWING
			: ANNOTATION_TYPE.POINTING;
	ANNOTATION_TYPE remoteAnnotation = scenario == SCENARIO.DRAWING ? ANNOTATION_TYPE.DRAWING
			: ANNOTATION_TYPE.POINTING;

	KetaiGesture gesture;
	KetaiSensor sensor;
	PVector orientation, remoteOrientation;

	float startSize, startAngle, endAngle, rotateAngle, step;
	int touchState;

	// main users color (red)
	int activeColor = color(255, 0, 80);
	// remote users color (blue)
	int remoteColor = color(100, 100, 255);

	float cwidth, cheight, cradius;
	int tubeRes = 32;
	float[] tubeX = new float[tubeRes + 1];
	float[] tubeY = new float[tubeRes + 1];
	PImage img;

	int x, y, px, py;
	float o;

	float touchX, touchY;

	// Screen and pad dimensions
	int myScreenWidth = 640;
	int myScreenHeight = 360;
	int padWidth = 1366;
	int padHeight = 187;
	boolean blinking, touched;

	// Touch events
	String touchEvent = ""; // string for the touch event type

	float x_start;
	float dx;
	float timer, r, ripplex, rippley, posx, posy;

	float hue = 0.0f;
	float l = 100;

	// scale factors
	float touchPadScaleX;
	float touchPadScaleY;
	float xpos, ypos;
	float previousX;
	float previousY;

	int moves = 0;
	int speed = 20;

	float FOVwidth = 557; // 557 pixels of the image is visible on the screen

	File file;
	Radar radar;
	Rectangle rectangle;
	Pointing pointing;
	Drawing drawing;
	// Connection connection = new Connection();
	Device device = new GlassDevice();
	Conn conn = new Conn(device);

	Status status;

	// ********************************************************************
	// ********************************************************************
	 public void onCreate(Bundle savedInstanceState) {
	 super.onCreate(savedInstanceState);
	 getWindow().addFlags(
	 android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	 }

	// ********************************************************************

	public void onResume() {
		println("onResume");
		super.onResume();

		file = new File();
		file.write("onResume");
	}

	public void onStop() {
		println("onStop");
		super.onStop();
	}

	public void onPause() {
		println("onPause");
		conn.onPause();

		super.onPause();

		file.write("onPause");
		file.destroy();
	}

	private String getPanoramaImageFileName() {
		 return "pano.jpg";

//		Bundle extras = this.getIntent().getExtras();
//		String PanoramaFile = extras.getString("PanoramaFile");
//
//		return PanoramaFile;
	}

	private PImage getPanoramaImage() {
		return loadImage(getPanoramaImageFileName());
	}

	public void setup() {
		background(0);

		conn.onResume();

		touchState = 0;

		img = getPanoramaImage();

		cwidth = (float) 360.0f;
		cheight = (float) img.height / (float) img.width * 360.0f;
		cradius = cwidth / (float) (2 * Math.PI);

		setupFOV();

		float angle = 360.0f / tubeRes;
		for (int i = 0; i <= tubeRes; i++) {
			tubeX[i] = cos(radians(i * angle));
			tubeY[i] = sin(radians(i * angle));
		}
		noStroke();

		gesture = new KetaiGesture(this);
		sensor = new KetaiSensor(this);
		sensor.start();

		orientation = new PVector();
		remoteOrientation = new PVector();

		// set the touch scale factor
		touchPadScaleX = (float) myScreenWidth / padWidth;
		touchPadScaleY = (float) myScreenHeight / padHeight;

		float imageScreenWidth = img.width * width / FOVwidth;

		radar = new Radar();
		rectangle = new Rectangle(imageScreenWidth);
		pointing = new Pointing();
		drawing = new Drawing();
		status = new Status();
	}

	public void setupFOV() {
		// float fov = radians(30);
		// float cameraZ = (height/2.0) / tan(fov/2.0);
		// perspective(fov, float(width)/float(height), cameraZ/10.0,
		// cameraZ*10.0);

		float fovFactor, degree, fov, cameraY, cameraZ, aspect, clippingFactor;

		// fovFactor = 80; //180
		// fovFactor = mouseX;
		// fovFactor = map(mouseX, 0, width, 70,400);

		degree = 30;
		// degree = map(mouseX, 0, width, 0, 5);

		fov = radians(degree);
		// fov = fovFactor/float(width) * PI/2;

		// println("degree: " + degree + " fov: " + fov);
		// println("fovFactor: " + fovFactor+ " fov: " + fov);

		cameraY = height / 2.0f;
		cameraZ = cameraY / tan(fov / 2.0f);
		aspect = PApplet.parseFloat(width) / PApplet.parseFloat(height);

		clippingFactor = 10.0f; // 30.0
		// clippingFactor = map (mouseX, 0, width, 10, 100);
		// println("clippingFactor: " + clippingFactor);

		perspective(fov, aspect, 1, cameraZ * clippingFactor);
	}

	public void draw() {
		clear();
		background(10);

		float z = 250; // 250 //1200
		z = 266;

		// z = z + map(touchX, 0, padWidth, -100, 100);
		// println("z:" + z + " touchX: " + touchX);

		translate(width / 2, height / 2);

		pushMatrix();
		setupFOV();
		translate(0, 0, z);
		drawPanorama();
		popMatrix();

		// switch(scenario)
		// {
		// case RADAR_BOX:
		// rectangle.drawRectangle(orientation, remoteOrientation);
		// radar.drawOldRadar();
		// break;
		//
		// case CONTEXT_COMPASS:
		radar.drawContextCompass();
		// break;
		//
		// case CENTERED_RADAR:
		// radar.drawCenteredRadar();
		// break;
		// }

		rectangle.drawCenterPoint(orientation, remoteOrientation);

		stroke(100);
		strokeWeight(10);

		status.drawStatus();
		conn.drawConnections();

		file.writeOrientation(orientation, remoteOrientation);

		conn.sendBufferMessages();
	}

	public void onOrientationEvent(float x, float y, float z, long time,
			int accuracy) {
		orientation.set(x, y, z);
		// println("orientaiton x: " + x);
	}

	public void drawPanorama() {
		pushMatrix();

		rotateY(radians(orientation.x + 90)); // roll

		// drawAxis();
		drawCylinder();
		drawPoints();

		popMatrix();
	}

	public void drawAxis() {
		strokeWeight(1);
		stroke(255, 0, 0);
		line(0, 0, 0, 100, 0, 0);

		stroke(0, 255, 0);
		line(0, 0, 0, 0, 100, 0);

		stroke(0, 0, 255);
		line(0, 0, 0, 0, 0, 100);
	}

	public void drawCylinder() {
		stroke(0);
		noStroke();
		beginShape(QUAD_STRIP);
		texture(img);

		float r = cradius;
		float h = cheight;
		for (int i = 0; i <= tubeRes; i++) {
			float x = tubeX[i] * r;
			float z = tubeY[i] * r;
			float u = img.width / tubeRes * i;
			vertex(x, -h / 2, z, u, 0);
			vertex(x, h / 2, z, u, img.height);
		}
		endShape();
	}

	public void drawPoints() {
		pushMatrix();

		switch (scenario) {
		case DRAWING:
			drawing.drawLocalDrawing();
			drawing.drawRemoteDrawing();
			break;

		case POINTING:
			pointing.drawLocalPointing();
			pointing.drawRemotePointing();
			break;

		case POINTING_DRAWING:
			drawLocalAnnotation();
			drawRemoteAnnotation();
			break;
		}

		popMatrix();

		// println("points: " + points.size() + " remotePoints: " +
		// remotePoints.size());
	}

	public void drawLocalAnnotation() {
		switch (localAnnotation) {
		case POINTING:
			pointing.drawLocalPointing();
			drawing.drawLocalDrawing();
			break;

		case DRAWING:
			drawing.drawLocalDrawing();
			break;
		}
	}

	public void drawRemoteAnnotation() {

		switch (remoteAnnotation) {
		case POINTING:
			pointing.drawRemotePointing();
			drawing.drawRemoteDrawing();
			break;

		case DRAWING:
			drawing.drawRemoteDrawing();
			break;
		}
	}

	public void drawCylinderLine(Point point) {
		pushMatrix();
		translate(0, -cheight / 2);

		strokeWeight(10);

		// print("drawCylinderLine");
		// point.print();

		PVector c1 = getCylinderPoint(point.a, point.b, point.o);
		PVector c2 = getCylinderPoint(point.x, point.y, point.o);

		line(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
		// drawLine(img, c1.x, c1.y, c2.x, c2.y);

		popMatrix();
	}

	public void drawFingerPoint(Point point) {
		pushMatrix();
		translate(0, -cheight / 2);

		strokeWeight(10);

		// print("drawCylinderLine");
		// point.print();

		PVector c1 = getCylinderPoint(point.a, point.b, point.o);
		PVector c2 = getCylinderPoint(point.x, point.y, point.o);

		// line(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
		strokeWeight(40);
		line((c2.x - 2), (c2.y - 2), (c2.z - 2), c2.x, c2.y, c2.z);
		// drawLine(img, c1.x, c1.y, c2.x, c2.y);

		popMatrix();
	}

	public PVector getCylinderPoint(float x0, float y0, float orientation) {
		float r = cradius;

		float FOVw = cradius * (float) Math.PI / 2;
		float offset = orientation - 45;
		float hOffset = map(45, 0, img.height, 0, cheight);

		float x = map(x0, 0, width, 0 + offset, FOVw + offset);
		float y = map(y0, 0, height, 0 + hOffset, cheight - hOffset);

		float f = x;

		PVector cPoint = new PVector();
		cPoint.x = r * cos(radians(f));
		cPoint.y = y;
		cPoint.z = r * sin(radians(f));

		// print("getCylinderPoint- x: " + x + " x0: " + x0 + " y: " + y +
		// " y0: " + y0 + " orientation: " + orientation + " f: " + f +
		// " result- x:" + cPoint.x + " y:" + cPoint.y + " z:" + cPoint.z);

		return cPoint;
	}

	long lastLongPress;

	// Glass Touch Events - reads from touch pad
	public boolean dispatchGenericMotionEvent(MotionEvent event) {
		touchX = event.getX(); // get x/y coords of touch event
		touchY = event.getY();

		int action = event.getActionMasked(); // get code for action
		int pointerCount = event.getPointerCount();
		long diff = System.currentTimeMillis() - lastLongPress;
		println("pointerCount: " + pointerCount + " diff: " + diff
				+ " action: " + action);

		if (diff > 400.0f) {
			println("llllllllllong");
			lastLongPress = System.currentTimeMillis();

			conn.sendImage(getPanoramaImageFileName());

			if (pointerCount == 2 && scenario == SCENARIO.POINTING_DRAWING) {
				println("switching local annotation type");
				localAnnotation = localAnnotation == ANNOTATION_TYPE.DRAWING ? ANNOTATION_TYPE.POINTING
						: ANNOTATION_TYPE.DRAWING;
			}

			if (pointerCount == 3) {
				if (dist(previousX, previousY, touchX, touchY) < 10.0f) {
					drawing.clearLocalDrawing();
					file.writeClearAnnotation();
					conn.sendClearDrawingMessage();
				}
				
				conn.sendImage(getPanoramaImageFileName());
			}
		}

		if (pointerCount == 1) {
			switch (action) { // let us know which action code shows up
			case MotionEvent.ACTION_DOWN:
				touchEvent = "DOWN";
				break;

			case MotionEvent.ACTION_MOVE:
				if (touchEvent == "DOWN") {

					Point p = new Point(previousX * touchPadScaleX, previousY
							* touchPadScaleY, touchX * touchPadScaleX, touchY
							* touchPadScaleY, orientation.x);
					onScreenTouched(p);
				}

				break;

			case MotionEvent.ACTION_UP:
				touchEvent = "UP";
				break;

			default:
				touchEvent = "OTHER (CODE " + action + ")"; // default text on
															// other event
			}

			// println("touchEvent: " + touchEvent);

			previousX = touchX;
			previousY = touchY;
		}

		return super.dispatchTouchEvent(event); // pass data along when done!
	}

	public void onScreenTouched(Point p) {

		switch (scenario) {
		case DRAWING:
			onAnnotationDraw(p);
			break;

		case POINTING:
			onAnnotationPoint(p);
			break;

		case POINTING_DRAWING:

			switch (localAnnotation) {

			case DRAWING:
				onAnnotationDraw(p);
				break;

			case POINTING:
				onAnnotationPoint(p);
				break;
			}

			break;
		}

		// print("capture point");
		// p.print();
	}

	public void onAnnotationDraw(Point p) {
		drawing.points.add(p);
		file.writeAnnotation(ANNOTATION_TYPE.DRAWING, p);
		conn.sendAnnotationMessage(p);
	}

	public void onAnnotationPoint(Point p) {
		pointing.pointingPoint.set(p);
		file.writeAnnotation(ANNOTATION_TYPE.POINTING, p);
		conn.sendAnnotationMessage(p);
	}

	public void onDoubleTap(float x, float y) {
		println("double");
		// circle = new Circle( x*touchPadScaleX, y*touchPadScaleY,
		// orientation.x);
	}

	public boolean surfaceTouchEvent(MotionEvent event) {
		super.surfaceTouchEvent(event);
		println("surfaceTouchEvent: ");

		float touchX = event.getX(); // get x/y coords of touch event
		float touchY = event.getY();

		int pointerCount = event.getPointerCount();
		println("pointerCount: " + pointerCount);

		return gesture.surfaceTouchEvent(event);
	}

	public void keyPressed() {
		int KEY_SWIPE_DOWN = 4;

		// tap on the touchpad to start and stop camera
		if (key == CODED) {
			if (keyCode == DPAD) {

				println("worldState " + touchState);
				touchState = touchState + 1;
				if (touchState > 1)
					touchState = 0;
			} else if (keyCode == KEY_SWIPE_DOWN) {
				println("swipe down");
			}
		}
	}

	public void onSaveInstanceState() {
		println("onSaveInstanceState");
	}

	public class Device {
		public final static String ONEPLUS_IP = "10.32.43.5";
		public final static String NEXUS10_IP = "10.32.14.13";
		public final static String GLASS_IP = "10.32.54.126";

		public String remoteIP;
		public int bgColor;
		public PImage img;

		public void drawBg() {
		}

		public void drawImage() {
			if (img != null) {
				image(img, 0, 0);
			}
		}

	}

	public class Nexus10Device extends Device {

		public Nexus10Device() {
			this.remoteIP = GLASS_IP;
		}

		public void drawBg() {
			background(255, 0, 0);
			drawImage();
		}

	}

	public class OnePlusDevice extends Device {

		public OnePlusDevice() {
			this.remoteIP = NEXUS10_IP;
		}

		public void drawBg() {
			background(0, 255, 0);
			drawImage();
		}

	}

	public class GlassDevice extends Device {

		public GlassDevice() {
			this.remoteIP = NEXUS10_IP;
		}

		public void drawBg() {
			background(0, 0, 255);
			drawImage();
		}

	}

	public class Conn {

		private static final int PORT = 11000;

		OscP5 oscP5tcpServer;
		OscP5 oscP5tcpClient;
		ArrayList bufferMessages = new ArrayList();

		long lastOrientationUpdate = 0;
		String CLEAR = "clear";

		public Conn(Device device) {

		}

		void drawConnections() {
			pushMatrix();
			ortho();

			textSize(30);
			fill(255, 0, 0);

			translate(-width / 2, -height / 2);

			int clientsCount = this.getConnectedClientsLength();
			text("connected: " + clientsCount, 30, 30);

			popMatrix();
		}

		void onResume() {

			oscP5tcpServer = new OscP5(this, PORT, OscP5.TCP);
			oscP5tcpClient = new OscP5(this, device.remoteIP, PORT, OscP5.TCP);

		}

		void pause(OscP5 oscP5) {
			oscP5.stop();
			oscP5.dispose();
			oscP5 = null;
		}

		void onPause() {
			pause(oscP5tcpServer);
			pause(oscP5tcpClient);
		}

		public TcpClient[] getConnectedClients() {
			if (!isStarted()) {
				return null;
			}

			return oscP5tcpServer.tcpServer().getClients();
		}

		public int getConnectedClientsLength() {
			if (!isStarted()) {
				return 0;
			}

			return oscP5tcpServer.tcpServer().getClients().length;
		}

		public boolean isStarted() {
			return oscP5tcpServer != null && oscP5tcpClient != null;
		}

		public boolean isServer() {
			return this.getConnectedClientsLength() > 0;
		}

		public void send(OscMessage message) {
			// todo:
			if (isServer()) {
				oscP5tcpServer.send(message);
			} else {
				oscP5tcpClient.send(message);
			}
		}

		public void sendImage(String imageFile) {
			println("sendImage: " + imageFile);
			
			byte[] imageBytes = loadBytes(imageFile);

			OscMessage myOscMessage = new OscMessage("/test");
			myOscMessage.add(100);
			myOscMessage.add(imageBytes);

			this.send(myOscMessage);
		}

		void oscEvent(OscMessage theMessage) {
			// messages.add("oscEvent");

			println("### got a message " + theMessage);

			if (theMessage.checkAddrPattern("/test")) {

				println("args: " + theMessage.arguments().length);
				// messages.add("args: " + theMessage.arguments().length);

				OscArgument arg0 = theMessage.get(0);
				// messages.add("arg0: "+ arg0.intValue());

				OscArgument arg1 = theMessage.get(1);
				// messages.add("arg1: "+ arg1.bytesValue());

				saveBytes("incoming-photo.jpg", arg1.bytesValue());
				device.img = loadImage("incoming-photo.jpg");

			}

			else

			if (theMessage.checkTypetag("fffffi")) {
				println(theMessage);

				Point p = new Point();

				p.a = theMessage.get(0).floatValue();
				p.b = theMessage.get(1).floatValue();
				p.x = theMessage.get(2).floatValue();
				p.y = theMessage.get(3).floatValue();
				p.o = theMessage.get(4).floatValue();
				int s = theMessage.get(5).intValue();

				println("s: " + s);

				ANNOTATION_TYPE remoteScenario = ANNOTATION_TYPE.values()[s];

				println("remoteScenario: " + remoteScenario);

				// p.scaleToScreen();

				switch (remoteScenario) {
				case DRAWING:
					drawing.remotePoints.add(p);
					remoteAnnotation = ANNOTATION_TYPE.DRAWING;
					break;

				case POINTING:
					pointing.remotePointingPoint.set(p);
					remoteAnnotation = ANNOTATION_TYPE.POINTING;
					break;

				default:
					println("invalid scenario:" + remoteScenario);
				}

				p.print();
				// p.draw();
			} else if (theMessage.checkTypetag("fff")) {
				float x = theMessage.get(0).floatValue();
				float y = theMessage.get(1).floatValue();
				float z = theMessage.get(2).floatValue();

				// float offset = -2.5625;

				// offset = map(mouseX, 0, width, -10, 10);
				// println("offset: " + offset + " x: " + x);

				remoteOrientation.set(x, y, z);
				// println("x: " + x + " y: " + y + " z: " + z);
			} else if (theMessage.checkTypetag("s")) {
				String command = theMessage.get(0).stringValue();
				if (command.equals(CLEAR)) {
					drawing.clearRemoteDrawing();
				}
			}
		}

		void sendClearDrawingMessage() {

			OscMessage myMessage = new OscMessage("AndroidData");
			myMessage.add(CLEAR);

			bufferMessages.add(myMessage);
		}

		void sendAnnotationMessage(Point p) {
			Point p2 = new Point(p);
			// p2.scaleTo640x360();

			OscMessage myMessage = new OscMessage("AndroidData");
			myMessage.add(p2.a);
			myMessage.add(p2.b);
			myMessage.add(p2.x);
			myMessage.add(p2.y);
			myMessage.add(p2.o);
			myMessage.add(localAnnotation.ordinal());

			bufferMessages.add(myMessage);

			// println("sending...");
		}

		void sendBufferMessages() {
			if (bufferMessages.size() > 0) {
				for (int i = 0; i < bufferMessages.size(); i++) {
					Object m = bufferMessages.get(i);
					if (m != null) {
						OscMessage myMessage = (OscMessage) m;
						if (myMessage != null) {

							this.send(myMessage);

						}
					}
				}

				bufferMessages.clear();
			}

			if (System.currentTimeMillis() - lastOrientationUpdate > 500) {
				// send the current orientation to the other device
				OscMessage myMessage = new OscMessage("AndroidData");
				myMessage.add(orientation.x);
				myMessage.add(orientation.y);
				myMessage.add(orientation.z);

				this.send(myMessage);

				lastOrientationUpdate = System.currentTimeMillis();
			}
		}
	}

	class Drawing {
		ArrayList points = new ArrayList();
		ArrayList remotePoints = new ArrayList();

		public void drawLocalDrawing() {
			// draw line
			stroke(activeColor);

			loop(points);
		}

		public void drawRemoteDrawing() {
			stroke(remoteColor);
			loop(remotePoints);
		}

		public void loop(ArrayList array) {
			for (int i = 0; i < array.size(); i++) {
				Object p = array.get(i);
				if (p != null) {
					Point dots = (Point) p;
					drawCylinderLine(dots);
				}
			}
		}

		public void clearLocalDrawing() {
			points.clear();
		}

		public void clearRemoteDrawing() {
			remotePoints.clear();
		}
	}

	class Pointing {
		Point pointingPoint = new Point();
		Point remotePointingPoint = new Point();

		// triangle parameters
		float offset = 10;
		float factor = 3;

		public void drawLocalPointing() {
			// draw finger point
			stroke(activeColor);
			drawFingerPoint(pointingPoint);
		}

		public void drawRemotePointing() {
			stroke(remoteColor);
			drawFingerPoint(remotePointingPoint);
		}

		public void drawFingerPoint(Point point) {
			pushMatrix();
			translate(0, -cheight / 2);

			strokeWeight(5);

			float angle = atan2((point.y - point.b), (point.x - point.a));

			PVector v1 = createVector(-offset * factor, -offset, point, angle);
			PVector v2 = createVector(0, 0, point, angle);
			PVector v3 = createVector(-offset * factor, +offset, point, angle);

			PVector c1 = getCylinderPoint(v1.x, v1.y, point.o);
			PVector c2 = getCylinderPoint(v2.x, v2.y, point.o);
			PVector c3 = getCylinderPoint(v3.x, v3.y, point.o);

			line(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
			line(c1.x, c1.y, c1.z, c3.x, c3.y, c3.z);
			line(c3.x, c3.y, c3.z, c2.x, c2.y, c2.z);

			popMatrix();
		}

		public PVector createVector(float x0, float y0, Point point, float angle) {
			PVector v = new PVector(x0, y0);
			v.rotate(angle);
			v.add(point.x, point.y, 0);

			return v;
		}
	}

	class Radar {

		// main users color (red)
		int activeColor = color(255, 0, 80);
		// remote users color (blue)
		int remoteColor = color(100, 100, 255);

		int ellipseWidth = height / 4;
		int pointerWidth = height / 3;

		public void drawContextCompass() {
			pushMatrix();
			ortho();

			stroke(activeColor);
			strokeWeight(1);
			line(-width / 3, -height / 3, width / 3, -height / 3);
			noStroke();
			translate(-width / 3, 0, 0);

			float ratio = (width / 3 * 2) / 360.0f;

			int rectWidth = width / 5;
			int rectHeight = height / 15;

			fill(activeColor, 200);
			ellipse(orientation.x * ratio, -height / 3, 8, rectHeight);
			fill(activeColor, 100);
			rect(orientation.x * ratio - rectWidth / 2, -height / 3
					- rectHeight / 2, rectWidth, rectHeight);

			fill(remoteColor, 200);
			ellipse(remoteOrientation.x * ratio, -height / 3, 8, rectHeight);
			fill(remoteColor, 100);
			rect(remoteOrientation.x * ratio - rectWidth / 2, -height / 3
					- rectHeight / 2, rectWidth, rectHeight);
			popMatrix();
		}

		public void drawCenteredRadar() {
			pushMatrix();
			ortho();

			// draw ellipses
			pushMatrix();
			// draw centerpoint
			stroke(activeColor);
			strokeWeight(4);
			fill(255, 50);
			ellipse(0, 0, ellipseWidth / 2, ellipseWidth / 2);

			popMatrix();

			// new active Radar middle point
			pushMatrix();
			noStroke();
			fill(activeColor, 150);
			rotateZ(radians(orientation.x));
			drawArc(pointerWidth, 55);
			popMatrix();

			// new active Radar area
			pushMatrix();
			noStroke();
			fill(activeColor, 30);
			rotateZ(radians(orientation.x));
			drawArc(ellipseWidth, 3);
			popMatrix();

			// new active Radar stroke
			pushMatrix();
			stroke(activeColor);
			strokeWeight(4);
			rotateZ(radians(orientation.x));
			drawArc(ellipseWidth, 3);
			popMatrix();

			// new remote radar middle point
			pushMatrix();
			noStroke();
			fill(remoteColor, 150);
			rotateZ(radians(remoteOrientation.x));
			drawArc(pointerWidth, 55);
			popMatrix();

			// new remote radar area
			pushMatrix();
			noStroke();
			fill(remoteColor, 50);
			rotateZ(radians(remoteOrientation.x));
			drawArc(ellipseWidth, 3);
			popMatrix();

			// new remote radar stroke
			pushMatrix();
			strokeWeight(4);
			stroke(remoteColor);
			noFill();
			rotateZ(radians(remoteOrientation.x));
			drawArc(ellipseWidth, 3);
			popMatrix();

			popMatrix();
		}

		public void drawArc(float arcSize, float arcStep) {
			startSize = arcSize;
			step = arcStep;
			startAngle = rotateAngle - PI / step;
			endAngle = rotateAngle + PI / step;
			arc(0, 0, startSize, startSize, startAngle, endAngle);
		}

		public void drawOldRadar() {
			pushMatrix();
			ortho();

			// draw centerpoint
			pushMatrix();
			stroke(255);
			strokeWeight(4);
			fill(255, 50);
			ellipse(0, 0, ellipseWidth / 2, ellipseWidth / 2);
			popMatrix();

			translate(width / 3, -height / 4);
			translate(25, -9, -20);

			// white radar
			pushMatrix();
			noStroke();
			fill(activeColor, 255);

			rotateZ(radians(orientation.x));
			drawArc(pointerWidth + 10, 55);
			popMatrix();

			pushMatrix();
			noStroke();
			fill(activeColor, 100);
			rotateZ(radians(orientation.x));
			drawArc(pointerWidth, 3);
			popMatrix();

			// turquoise radar

			pushMatrix();
			// strokeWeight(5);
			noStroke();
			fill(remoteColor, 255);
			rotateZ(radians(remoteOrientation.x));
			drawArc(pointerWidth + 10, 55);
			popMatrix();

			pushMatrix();
			// strokeWeight(5);
			noStroke();
			fill(remoteColor, 100);
			rotateZ(radians(remoteOrientation.x));
			drawArc(pointerWidth, 3);
			popMatrix();

			int offset = 20;

			pushMatrix();
			noFill();
			stroke(255, 50);
			strokeWeight(4);
			ellipse(0, 0, ellipseWidth + offset, ellipseWidth + offset);
			strokeWeight(2);
			ellipse(0, 0, ellipseWidth * 3 / 4 + offset, ellipseWidth * 3 / 4
					+ offset);
			strokeWeight(1);
			ellipse(0, 0, ellipseWidth / 2 + offset, ellipseWidth / 2 + offset);
			ellipse(0, 0, ellipseWidth * 1 / 3 + offset, ellipseWidth * 1 / 3
					+ offset);
			ellipse(0, 0, ellipseWidth * 1 / 4 + offset, ellipseWidth * 1 / 4
					+ offset);
			fill(255);
			ellipse(0, 0, ellipseWidth * 1 / 8, ellipseWidth * 1 / 8);
			popMatrix();

			popMatrix();
		}
	}

	class Rectangle {
		int remoteColor = color(100, 100, 255);
		float padding = 10;
		float imageScreenWidth;

		Rectangle(float imageScreenWidth) {
			this.imageScreenWidth = imageScreenWidth;
			// padding = map(mouseX, 0, width, 0, 100);
			// println("padding: " + padding);
		}

		public float calculateOffset(PVector orientation,
				PVector remoteOrientation) {
			float diff = orientation.x - remoteOrientation.x;
			diff = diff > 180 ? diff - 360 : diff;

			// println("orientation.x: " + orientation.x + " diff: " + diff+
			// " offsetX: " + offsetX);

			float offsetX = map(-diff, -180.0f, 180.0f, -imageScreenWidth / 2,
					imageScreenWidth / 2);

			return offsetX;
		}

		public void pre(PVector orientation, PVector remoteOrientation) {
			float offsetX = calculateOffset(orientation, remoteOrientation);

			pushMatrix();
			ortho();
			translate(offsetX, 0, 0);
		}

		public void post() {
			popMatrix();
		}

		public void drawRectangle(PVector orientation, PVector remoteOrientation) {
			pre(orientation, remoteOrientation);

			drawRectangle();

			post();
		}

		public void drawCenterPoint(PVector orientation,
				PVector remoteOrientation) {
			pre(orientation, remoteOrientation);

			drawCenterPoint();

			post();
		}

		public void drawRectangle() {
			// draw rectangle
			strokeWeight(10);
			stroke(remoteColor, 150);
			noFill();
			rect(-width / 2 + padding, -height / 2 + padding, width - padding
					* 2, height - padding * 2);
		}

		public void drawCenterPoint() {
			// draw center point
			strokeWeight(1);
			fill(remoteColor, 150);
			ellipse(0, 0, height / 8, height / 8);
		}
	}

	class Status {
		PImage imgDrawing, imgPointing;
		int imgPadding = 10;

		Status() {
			imgDrawing = loadImage("icon-drawing.png");
			imgPointing = loadImage("icon-pointing.png");
		}

		public void drawStatus() {
			pushMatrix();
			ortho();

			translate(-width / 2, height / 2);
			textSize(15);
			fill(255, 0, 0);
			text("scenario: " + scenario, 0, 0);

			translate(0, -10);
			text("local: " + drawing.points.size(), 0, 0);

			translate(0, -10);
			text("remote: " + drawing.remotePoints.size(), 0, 0);

			popMatrix();

			drawAnnotationTypeIcon();
		}

		public void drawAnnotationTypeIcon() {
			pushMatrix();
			ortho();

			PImage img = getIconImage();

			// float x = map(mouseX, 0, width, 0, 100);
			// println("x: " + x);

			float x = 15; // offset
			float y = 5;

			translate(width / 2, height / 2);
			image(img, -img.width - imgPadding - x, -img.height - imgPadding
					- y);

			popMatrix();
		}

		public PImage getIconImage() {
			return localAnnotation == ANNOTATION_TYPE.DRAWING ? imgDrawing
					: imgPointing;
		}
	}

	class Circle {
		float a, b, o;

		Circle() {
		}

		Circle(float apos, float bpos, float orientation) {
			a = apos;
			b = bpos;
			o = orientation;
		}

		public void setValues(float apos, float bpos, float orientation) {
			a = apos;
			b = bpos;
			o = orientation;
		}

		public void print() {
			println("circle - a:" + this.a + " b:" + this.b + " o:" + this.o);
		}

	}

	class File {

		PrintWriter output;
		long lastUpdate = 0;

		File() {
			int D = day(); // Values from 1 - 31
			int M = month(); // Values from 1 - 12
			int Y = year(); // 2003, 2004, 2005, etc.

			int ms = millis();
			int s = second(); // Values from 0 - 59
			int m = minute(); // Values from 0 - 59
			int h = hour(); // Values from 0 - 23

			String fileName = String.format("Experiment_%s%s%s_%s%s%s_%s.csv",
					Y, M, D, h, m, s, ms);
			String basePath = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
			String directory = "/panorama_experiment/";
			String filePath = basePath + directory + fileName;

			output = createWriter(filePath);
		}

		public boolean isReady() {
			if (System.currentTimeMillis() - lastUpdate > 100) {
				lastUpdate = System.currentTimeMillis();
				return true;
			}

			return false;
		}

		public void writeTime() {
			int D = day(); // Values from 1 - 31
			int M = month(); // Values from 1 - 12
			int Y = year(); // 2003, 2004, 2005, etc.

			int s = second(); // Values from 0 - 59
			int m = minute(); // Values from 0 - 59
			int h = hour(); // Values from 0 - 23

			output.print(String.format("GLASS,%s-%s-%s %s:%s:%s", Y, M, D, h,
					m, s));
		}

		public void write(String text) {
			writeTime();
			output.print(String.format(",%s", text));
			output.println();
			output.flush();
		}

		public void writeVector(PVector vector, String name) {
			writeTime();
			output.print(String.format(",%s,%s,%s,%s", name, vector.x,
					vector.y, vector.z));
		}

		public void writeOrientation(PVector local, PVector remote) {
			if (isReady()) {
				writeVector(local, "local");
				writeVector(remote, "remote");
				output.println();
				output.flush();
			}
		}

		public void writeAnnotation(ANNOTATION_TYPE annotation, Point point) {
			writeTime();
			output.print(String.format(",%s,%s,%s,%s,%s,%s", annotation,
					point.a, point.b, point.x, point.y, point.o));
			output.println();
			output.flush();
		}

		public void writeClearAnnotation() {
			writeTime();
			output.print(String.format(",CLEAR"));
			output.println();
			output.flush();
		}

		public void destroy() {
			output.flush(); // Writes the remaining data to the file
			output.close(); // Finishes the file
		}
	}

	class Point {
		float a, b, x, y, o;

		Point() {
		}

		Point(Point p) {
			set(p);
		}

		Point(float apos, float bpos, float xpos, float ypos, float orientation) {
			a = apos;
			b = bpos;
			x = xpos;
			y = ypos;
			o = orientation;
		}

		public void set(Point p) {
			a = p.a;
			b = p.b;
			x = p.x;
			y = p.y;
			o = p.o;
		}

		public void setValues(float apos, float bpos, float xpos, float ypos,
				float orientation) {
			a = apos;
			b = bpos;
			x = xpos;
			y = ypos;
			o = orientation;
		}

		public void print() {
			println("point - a:" + this.a + " b:" + this.b + " x:" + this.x
					+ " y:" + this.y + " o:" + this.o);
		}

		public void draw() {
			line(a, b, x, y);
		}
	}

	public int sketchWidth() {
		return 640;
	}

	public int sketchHeight() {
		return 360;
	}

	public String sketchRenderer() {
		return P3D;
	}
}
