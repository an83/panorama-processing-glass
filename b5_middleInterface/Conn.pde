
import oscP5.*;
import netP5.*;

public class Conn{

  private static final int PORT = 11000;
  
  OscP5 oscP5tcpServer;
  OscP5 oscP5tcpClient;
  ArrayList bufferMessages = new ArrayList();
  
  long lastOrientationUpdate = 0;
  String CLEAR = "clear";

  public Conn(Device device){
    
  }
  
  void onResume(){
    
    oscP5tcpServer = new OscP5(this, PORT, OscP5.TCP);
    oscP5tcpClient = new OscP5(this, device.remoteIP, PORT, OscP5.TCP);
  
  }
  
  void pause(OscP5 oscP5){
    oscP5.stop();
    oscP5.dispose();
    oscP5 = null;
  }

  void onPause() {
    pause(oscP5tcpServer);
    pause(oscP5tcpClient);
  }
  
  
    void drawConnections() {
      pushMatrix();
      ortho();

      textSize(30);
      fill(255, 0, 0);

      translate(-width / 2, -height / 2);

      int clientsCount = this.getConnectedClientsLength();
      text("connected: " + clientsCount, 30, 30);

      popMatrix();
    }
  
  public int getConnectedClientsLength(){
    if(!isStarted()){
      return 0;
    }
    
    return oscP5tcpServer.tcpServer().getClients().length;
  }
  
  public boolean isStarted(){
    return oscP5tcpServer != null && oscP5tcpClient != null;
  }
  
  public boolean isServer(){  
    return this.getConnectedClientsLength() > 0;
  }
  
  public void send(OscMessage message){
    if(isServer()){
      oscP5tcpServer.send(message);
    }
    else{
      oscP5tcpClient.send(message);
    }
  }
  
//  public void sendMessage(){
//    OscMessage myOscMessage = new OscMessage("/test");
//    myOscMessage.add(100);
//    myOscMessage.add(imgBytes);
//    
//    this.send(myOscMessage);
//  }



  void oscEvent(OscMessage theMessage) {
//    messages.add("oscEvent");
    
    println("### got a message " + theMessage);
    
    if(theMessage.checkAddrPattern("/test")) {
     
      println("args: " + theMessage.arguments().length);
//      messages.add("args: " + theMessage.arguments().length);
      
      OscArgument arg0 = theMessage.get(0);
//      messages.add("arg0: "+ arg0.intValue());
      
      OscArgument arg1 = theMessage.get(1);
//      messages.add("arg1: "+ arg1.bytesValue());
      
      saveBytes("incoming-photo.jpg", arg1.bytesValue());
      device.img = loadImage("incoming-photo.jpg"); 
      
    }
    
    else 
    
    if (theMessage.checkTypetag("fffffi"))
    {
      println(theMessage);

      Point p = new Point();

      p.a = theMessage.get(0).floatValue();
      p.b = theMessage.get(1).floatValue();
      p.x = theMessage.get(2).floatValue();
      p.y = theMessage.get(3).floatValue();
      p.o = theMessage.get(4).floatValue();
      int s = theMessage.get(5).intValue();

      println("s: " + s);

      ANNOTATION_TYPE remoteScenario = ANNOTATION_TYPE.values()[s];

      println("remoteScenario: " + remoteScenario);

      p.scaleToScreen();

      switch(remoteScenario)
      {
      case DRAWING:
        drawing.remotePoints.add(p);
        remoteAnnotation = ANNOTATION_TYPE.DRAWING;
        break;

      case POINTING:
        pointing.remotePointingPoint.set(p);      
        remoteAnnotation = ANNOTATION_TYPE.POINTING;
        break;

      default:
        println("invalid scenario:" + remoteScenario);
      }

      p.print();
      //    p.draw();
    }
    else if (theMessage.checkTypetag("fff"))
    {
      float x = theMessage.get(0).floatValue();
      float y = theMessage.get(1).floatValue();
      float z = theMessage.get(2).floatValue();

      //    float offset = -2.5625;

      //    offset = map(mouseX, 0, width, -10, 10);    
      //    println("offset: " + offset + " x: " + x);

      remoteOrientation.set(x, y, z);
      //    println("x: " + x + " y: " + y + " z: " + z);
    }
    else if (theMessage.checkTypetag("s")) {
      String command = theMessage.get(0).stringValue();
      if (command.equals(CLEAR)) {
        drawing.clearRemoteDrawing();
      }
    }
  }
  
  

  void sendClearDrawingMessage() {  

    OscMessage myMessage = new OscMessage("AndroidData"); 
    myMessage.add(CLEAR);

    bufferMessages.add(myMessage);
  }
  
  void sendAnnotationMessage(Point p) {  
    Point p2 = new Point(p);
    p2.scaleTo640x360();    

    OscMessage myMessage = new OscMessage("AndroidData"); 
    myMessage.add(p2.a);
    myMessage.add(p2.b);
    myMessage.add(p2.x);
    myMessage.add(p2.y);
    myMessage.add(p2.o);
    myMessage.add(localAnnotation.ordinal());

    bufferMessages.add(myMessage); 

    //    println("sending...");
  }
  
  
  void sendBufferMessages() {
    if ( bufferMessages.size()>0) {
      for (int i=0; i<bufferMessages.size();i++) {        
        Object  m = bufferMessages.get(i);
        if (m!= null) {
          OscMessage myMessage = (OscMessage)m;
          if (myMessage != null) {
            
            this.send(myMessage);
            
          }
        }
      }

      bufferMessages.clear();
    }

    if (System.currentTimeMillis() - lastOrientationUpdate > 500) {
      //send the current orientation to the other device
      OscMessage myMessage = new OscMessage("AndroidData"); 
      myMessage.add(orientation.x);
      myMessage.add(orientation.y);
      myMessage.add(orientation.z);
      
      
      this.send(myMessage);


      lastOrientationUpdate = System.currentTimeMillis();
    }
  }
}
