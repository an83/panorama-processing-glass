public class Device{
  public final static String ONEPLUS_IP = "10.32.43.5";
  public final static String NEXUS10_IP = "10.32.14.13";
  public final static String GLASS_IP   = "10.32.54.126";
   
  public String remoteIP;
  public int bgColor;  
  public PImage img;
  
  public void drawBg(){}
  
  public void drawImage(){
    if(img!=null){
      image(img,0,0);
    }
  }
  
}

public class Nexus10Device extends Device{
  
  public Nexus10Device(){
    this.remoteIP = GLASS_IP;
  }
  
  
  public void drawBg(){
    background(255,0,0);
    drawImage();
  }
  
}


public class OnePlusDevice extends Device{
  
  public OnePlusDevice(){
    this.remoteIP = NEXUS10_IP;
  }
  
  public void drawBg(){
    background(0,255,0);
    drawImage();
  }
  
}

public class GlassDevice extends Device{
  
  public GlassDevice(){
    this.remoteIP = NEXUS10_IP;
  }
  
  public void drawBg(){
    background(0,0,255);
    drawImage();
  }
  
}
