class Rectangle
{
  color remoteColor = color(100, 100, 255);
  float padding = 10;
  float imageScreenWidth;

  Rectangle(float imageScreenWidth)
  {
    this.imageScreenWidth = imageScreenWidth;
    //  padding = map(mouseX, 0, width, 0, 100); 
    //  println("padding: " + padding);
  }

  float calculateOffset(PVector orientation, PVector remoteOrientation)
  {
    float diff = orientation.x - remoteOrientation.x;  
    diff = diff > 180? diff - 360 : diff;  

    //  println("orientation.x: " + orientation.x + " diff: " + diff+ " offsetX: " + offsetX);

    float offsetX = map(-diff, -180.0, 180.0, -imageScreenWidth/2, imageScreenWidth/2);

    return offsetX;
  } 

  void pre()
  {
    pushMatrix();
    ortho();
  }
  
  void post()
  {
    popMatrix();
  }
  
  void drawRectangle(PVector orientation, PVector remoteOrientation)
  {
    pre();    
    
    drawLocalRectangle();    
    
    float offsetX = calculateOffset(orientation, remoteOrientation);    
    translate(offsetX, 0, 0);   
    
    drawRectangle();

    post();
  }
  
  void drawCenterPoint(PVector orientation, PVector remoteOrientation)
  {
    pre();    
        
    drawLocalCenterPoint();
    
    float offsetX = calculateOffset(orientation, remoteOrientation);
    translate(offsetX, 0, 0);   
    
    drawCenterPoint();

    post();
  }
  
  void drawLocalRectangle() {
    //draw rectangle
    strokeWeight(10);
    stroke(activeColor, 150);
    noFill();
    rect(-width/2+padding, -height/2+padding, width-padding*2, height-padding*2);
  }

  void drawRectangle() {
    //draw rectangle
    strokeWeight(10);
    stroke(remoteColor, 150);
    noFill();
    rect(-width/2+padding, -height/2+padding, width-padding*2, height-padding*2);
  }

  void drawLocalCenterPoint()
  {    
    //draw center point
    strokeWeight(1);  
    fill(activeColor, 150);
    ellipse(0, 0, height/8, height/8);
  }

  void drawCenterPoint()
  {
    //draw center point
    strokeWeight(1);  
    fill(remoteColor, 150);
    ellipse(0, 0, height/8, height/8);
  }
}

