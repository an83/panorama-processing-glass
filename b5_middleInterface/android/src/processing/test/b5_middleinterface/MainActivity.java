package processing.test.b5_middleinterface;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.os.Build;

public class MainActivity extends Activity {

	static TextView txtLocalIP;
	static EditText remoteIP;
	static ToggleButton tglCenteredRadar, tglBox, tglContextCompass,
			tglPointing, tglDrawing, tglPointingDrawing;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}


	}

	private static final String PATTERN = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
			+ "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

	public static boolean validate(final String ip) {
		Pattern pattern = Pattern.compile(PATTERN);
		Matcher matcher = pattern.matcher(ip);
		return matcher.matches();
	}

	public void onButtonClick(View view) {
		Intent intent = new Intent(this, b5_middleInterface.class);

		String ip = remoteIP.getText().toString();
		if (validate(ip)) {

			intent.putExtra(Extra.EXTRA_IP, ip);

			if (tglBox.isChecked())
				intent.putExtra(SCENARIO.RADAR_BOX.toString(), true);
			if (tglCenteredRadar.isChecked())
				intent.putExtra(SCENARIO.CENTERED_RADAR.toString(), true);
			if (tglContextCompass.isChecked())
				intent.putExtra(SCENARIO.CONTEXT_COMPASS.toString(), true);

			if (tglPointing.isChecked())
				intent.putExtra(SCENARIO.POINTING.toString(), true);
			if (tglDrawing.isChecked())
				intent.putExtra(SCENARIO.DRAWING.toString(), true);

			startActivity(intent);
		}
		else{
			Toast.makeText(getApplicationContext(), "Invalid IP address" , Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
		String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
		Log.v("TAG", "ip: " + ip);

		txtLocalIP.setText(ip);
		if(remoteIP.getText().toString().isEmpty()){
			remoteIP.setText(ip.replaceAll(".\\d+$", "."));	
		}		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);

			txtLocalIP = (TextView) rootView.findViewById(R.id.txtLocalIP);
			remoteIP = (EditText) rootView.findViewById(R.id.remoteIP);

			tglCenteredRadar = (ToggleButton) rootView
					.findViewById(R.id.tglCenteredRadar);
			tglBox = (ToggleButton) rootView.findViewById(R.id.tglBox);
			tglContextCompass = (ToggleButton) rootView
					.findViewById(R.id.tglContextCompass);
			tglPointing = (ToggleButton) rootView
					.findViewById(R.id.tglPointing);
			tglDrawing = (ToggleButton) rootView.findViewById(R.id.tglDrawing);


			
			
			return rootView;
		}

	}

}
