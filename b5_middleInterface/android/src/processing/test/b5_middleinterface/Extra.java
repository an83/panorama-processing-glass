package processing.test.b5_middleinterface;

import android.app.Activity;
import android.os.Bundle;

public class Extra {
	public final static String EXTRA_IP = "EXTRA_IP";
	
	public static boolean check(Activity activitiy, Object param){

		Bundle extras = activitiy.getIntent().getExtras();
		if (extras != null) {
			return extras.containsKey(param.toString());		    
		}
		
		return false;
	}
	public static String get(Activity activitiy, Object param){

		Bundle extras = activitiy.getIntent().getExtras();
		return extras.getString(param.toString());
	}
}
