package processing.test.b5_middleinterface;

import processing.core.*;
import ketai.sensors.*;
import ketai.ui.*;
import android.util.Log;
import android.view.*;
import android.widget.Toast;
import oscP5.*;
import netP5.*;
import android.os.Environment;

import java.util.ArrayList;
import java.io.PrintWriter;

public class b5_middleInterface extends PApplet {

	String REMOTE_ADDRESS = "10.1.1.1"; // glass IP address
	
	private final String TAG = b5_middleInterface.class.getSimpleName();
	SCENARIO scenario;
	ANNOTATION_TYPE localAnnotation, remoteAnnotation;
		

	KetaiGesture gesture;
	KetaiSensor sensor;
	PVector orientation, remoteOrientation;

	// main users color (red)
	int activeColor = color(255, 0, 80);
	// remote users color (blue)
	int remoteColor = color(100, 100, 255);

	float startSize, startAngle, endAngle, rotateAngle;
	float step;

	float FOVwidth = 557; // 557 pixels of the image is visible on the screen

	float cwidth, cheight, cradius;
	int tubeRes = 32;
	float[] tubeX = new float[tubeRes + 1];
	float[] tubeY = new float[tubeRes + 1];
	PImage img;

	// touch events
	float startX, startY;
	int touchState;

	int x, y, px, py;

	File file;
	Radar radar;
	Rectangle rectangle;
	Pointing pointing;
	Drawing drawing;
	Connection connection = new Connection();
	Status status;

	public void onResume() {
		println("onResume");
		super.onResume();

		Log.v(TAG, "init connection...");
		if(Extra.check(this, Extra.EXTRA_IP)){
			REMOTE_ADDRESS = Extra.get(this, Extra.EXTRA_IP);
			Log.v(TAG, "ip: " + REMOTE_ADDRESS);
		}
		
		connection.onResume();

		file = new File();
		file.write("onResume");
	}

	public void onPause() {
		println("onPause");

		connection.onPause();

		file.write("onPause");
		file.destroy();

		super.onPause();
	}

	public boolean sketchFullScreen() {
		return true;
	}

	public void setup() {
		println("setup");
		scenario = SCENARIO.POINTING_DRAWING;
		if(Extra.check(this, SCENARIO.POINTING)) scenario = SCENARIO.POINTING;
		if(Extra.check(this, SCENARIO.DRAWING)) scenario = SCENARIO.DRAWING;		
		if(Extra.check(this, SCENARIO.POINTING) &&
				Extra.check(this, SCENARIO.DRAWING)) scenario = SCENARIO.POINTING_DRAWING;
			
		localAnnotation = scenario == SCENARIO.DRAWING ? ANNOTATION_TYPE.DRAWING
				: ANNOTATION_TYPE.POINTING;
		remoteAnnotation = scenario == SCENARIO.DRAWING ? ANNOTATION_TYPE.DRAWING
				: ANNOTATION_TYPE.POINTING;

		
		orientation(LANDSCAPE);
		background(0);
		// size(640, 360, P3D);

		touchState = 0;

		setupFOV();

		img = loadImage("pano.jpg");

		cwidth = (float) 360.0f;
		cheight = (float) img.height / (float) img.width * 360.0f;
		cradius = cwidth / (float) (2 * Math.PI);

		println("cheight: " + cheight + " cradius: " + cradius
				+ " img.height: " + img.height + " img.width: " + img.width);

		float angle = 360.0f / tubeRes;
		for (int i = 0; i <= tubeRes; i++) {
			tubeX[i] = cos(radians(i * angle));
			tubeY[i] = sin(radians(i * angle));
		}
		noStroke();

		gesture = new KetaiGesture(this);
		sensor = new KetaiSensor(this);
		sensor.start();
		orientation = new PVector();
		remoteOrientation = new PVector();

		pointing = new Pointing();
		drawing = new Drawing();
		status = new Status();

		float imageScreenWidth = img.width * width / FOVwidth;

		radar = new Radar();
		rectangle = new Rectangle(imageScreenWidth);

		// Point p = new Point(0, 0, width, height, 90);
		// points.add(p);
	}

	float eyeX, eyeY, eyeZ;
	float centerX, centerY, centerZ;

	public void setupFOV() {
		float fovFactor, degree, fov, cameraY, cameraZ, aspect, clippingFactor;

		// fovFactor = 80; //180
		// fovFactor = mouseX;
		// fovFactor = map(mouseX, 0, width, 70,400);

		degree = 30;
		// degree = map(mouseX, 0, width, 0, 5);

		fov = radians(degree);
		// fov = fovFactor/float(width) * PI/2;

		// println("degree: " + degree + " fov: " + fov);
		// println("fovFactor: " + fovFactor+ " fov: " + fov);

		cameraY = height / 2.0f;
		cameraZ = cameraY / tan(degree / 2.0f);
		aspect = PApplet.parseFloat(width) / PApplet.parseFloat(height);

		clippingFactor = 10.0f; // 30.0
		// clippingFactor = map (mouseX, 0, width, 10, 100);
		// println("clippingFactor: " + clippingFactor);

		perspective(fov, aspect, 1, cameraZ * clippingFactor);
	}

	public void draw() {
		clear();
		background(10);

		float z = 1245; // 250 //1200
		z = 1254;
		// z = z + map(mouseX, 0, width, -100, 100);
		// println("z:" + z);

		translate(width / 2, height / 2);

		pushMatrix();
		setupFOV();
		translate(0, 0, z);
		drawPanorama();
		popMatrix();

		if (Extra.check(this, SCENARIO.RADAR_BOX)) {
			rectangle.drawRectangle(orientation, remoteOrientation);
			radar.drawOldRadar();
		}
		if (Extra.check(this, SCENARIO.CONTEXT_COMPASS)) {
			radar.drawContextCompass();
		}
		if (Extra.check(this, SCENARIO.CENTERED_RADAR)) {
			radar.drawCenteredRadar();
		}
		
		rectangle.drawCenterPoint(orientation, remoteOrientation);

		status.drawStatus();

		file.writeOrientation(orientation, remoteOrientation);

		connection.sendBufferMessages();
	}

	public void onAnnotationDraw(Point p) {
		drawing.points.add(p);
		file.writeAnnotation(ANNOTATION_TYPE.DRAWING, p);
		connection.sendAnnotationMessage(p);
	}

	public void onAnnotationPoint(Point p) {
		pointing.pointingPoint.set(p);
		file.writeAnnotation(ANNOTATION_TYPE.POINTING, p);
		connection.sendAnnotationMessage(p);
	}

	public void onScreenTouched() {
		println("onScreenTouched");

		Point p = new Point(pmouseX, pmouseY, mouseX, mouseY, orientation.x);

		switch (scenario) {
		case DRAWING:
			onAnnotationDraw(p);
			break;

		case POINTING:
			onAnnotationPoint(p);
			break;

		case POINTING_DRAWING:

			switch (localAnnotation) {

			case DRAWING:
				onAnnotationDraw(p);
				break;

			case POINTING:
				onAnnotationPoint(p);
				break;
			}

			break;
		}

		// print("capture point");
		// p.print();
	}

	public void onOrientationEvent(float x, float y, float z, long time,
			int accuracy) {
		orientation.set(x, y, z);
		// println("orientaiton x: " + x);
	}

	public void drawPanorama() {
		pushMatrix();

		rotateY(radians(orientation.x + 90)); // roll

		// drawAxis();
		drawCylinder();
		drawPoints();

		popMatrix();
	}

	public void drawPoints() {
		pushMatrix();

		switch (scenario) {
		case DRAWING:
			drawing.drawLocalDrawing();
			drawing.drawRemoteDrawing();
			break;

		case POINTING:
			pointing.drawLocalPointing();
			pointing.drawRemotePointing();
			break;

		case POINTING_DRAWING:
			drawLocalAnnotation();
			drawRemoteAnnotation();
			break;
		}

		popMatrix();
	}

	public void drawLocalAnnotation() {
		switch (localAnnotation) {
		case POINTING:
			pointing.drawLocalPointing();
			drawing.drawLocalDrawing();
			break;

		case DRAWING:
			drawing.drawLocalDrawing();
			break;
		}
	}

	public void drawRemoteAnnotation() {

		switch (remoteAnnotation) {
		case POINTING:
			pointing.drawRemotePointing();
			drawing.drawRemoteDrawing();
			break;

		case DRAWING:
			drawing.drawRemoteDrawing();
			break;
		}
	}

	public void drawAxis() {
		pushMatrix();

		strokeWeight(1);
		stroke(255, 0, 0);
		line(0, 0, 0, 100, 0, 0);

		stroke(0, 255, 0);
		line(0, 0, 0, 0, 100, 0);

		stroke(0, 0, 255);
		line(0, 0, 0, 0, 0, 100);

		popMatrix();
	}

	public void drawCylinder() {
		stroke(0);
		noStroke();
		beginShape(QUAD_STRIP);
		texture(img);
		fill(255, 255, 255);

		float r = cradius;
		float h = cheight;
		for (int i = 0; i <= tubeRes; i++) {
			float x = tubeX[i] * r;
			float z = tubeY[i] * r;
			float u = img.width / tubeRes * i;
			vertex(x, -h / 2, z, u, 0);
			vertex(x, h / 2, z, u, img.height);
		}
		endShape();
	}

	public void drawCylinderLine(Point point) {
		pushMatrix();
		translate(0, -cheight / 2);

		strokeWeight(10);

		// print("drawCylinderLine");
		// point.print();

		PVector c1 = getCylinderPoint(point.a, point.b, point.o);
		PVector c2 = getCylinderPoint(point.x, point.y, point.o);

		line(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);

		strokeWeight(1);
		popMatrix();
	}

	public PVector getCylinderPoint(float x0, float y0, float orientation) {
		float r = cradius;

		float FOVw = cradius * (float) Math.PI / 2;

		float offset = orientation - 45;
		// offset = 0;

		float hOffset = map(45, 0, img.height, 0, cheight);

		float x = map(x0, 0, width, 0 + offset, FOVw + offset);
		float y = map(y0, 0, height, 0 + hOffset, cheight - hOffset);

		float f = x;

		PVector cPoint = new PVector();
		cPoint.x = r * cos(radians(f));
		cPoint.y = y;
		cPoint.z = r * sin(radians(f));

		// print("getCylinderPoint- x: " + x + " x0: " + x0 + " y: " + y +
		// " y0: " + y0 + " orientation: " + orientation + " f: " + f +
		// " result- x:" + cPoint.x + " y:" + cPoint.y + " z:" + cPoint.z);

		return cPoint;
	}

	long lastLongPress;

	public void onLongPress(android.view.MotionEvent arg0) {
		println("onLongPress");
	}

	final GestureDetector gestureDetector = new GestureDetector(
			new GestureDetector.SimpleOnGestureListener() {
				public void onLongPress(MotionEvent event) {
					int pointerCount = event.getPointerCount();

					println("Longpress detected. pointerCount: " + pointerCount);
				}
			});

	public boolean onTouchEvent(MotionEvent event) {
		return gestureDetector.onTouchEvent(event);
	};

	public void mouseDragged() // (19)
	{
		onScreenTouched();
	}

	public boolean surfaceTouchEvent(MotionEvent event) {
		boolean result = super.surfaceTouchEvent(event);
		int action = event.getActionMasked(); // get code for action

		float touchX = event.getX(); // get x/y coords of touch event
		float touchY = event.getY();

		int pointerCount = event.getPointerCount();
		long diff = System.currentTimeMillis() - lastLongPress;
		println("pointerCount: " + pointerCount + " diff: " + diff
				+ " action: " + action);
		if (diff > 400.0f) {
			println("llllllllllong");
			lastLongPress = System.currentTimeMillis();

			if (pointerCount == 2 && scenario == SCENARIO.POINTING_DRAWING) {
				println("switching local annotation type");
				localAnnotation = localAnnotation == ANNOTATION_TYPE.DRAWING ? ANNOTATION_TYPE.POINTING
						: ANNOTATION_TYPE.DRAWING;
			}

			if (pointerCount == 3) {
				if (dist(startX, startY, touchX, touchY) < 10.0f) {
					drawing.clearLocalDrawing();
					file.writeClearAnnotation();
					connection.sendClearDrawingMessage();
				}
			}
		}

		switch (action) {

		case MotionEvent.ACTION_DOWN:
			startX = touchX;
			startY = touchY;
			break;

		case MotionEvent.ACTION_UP:
			break;
		}
		return gesture.surfaceTouchEvent(event);
	}

	class Connection {
		private final String TAG = Connection.class.getSimpleName();

		String CLEAR = "clear";
		
		OscP5 oscP5;
		NetAddress remoteLocation;
		long lastOrientationUpdate = 0;
		ArrayList bufferMessages = new ArrayList();

		Connection() {
		}

		public void onResume() {
			Log.v(TAG, "connection onResume");
			
			remoteLocation = new NetAddress(REMOTE_ADDRESS, 32000);			
			oscP5 = new OscP5(this, 12000);
		}

		public void onPause() {
			oscP5.stop();
			oscP5.dispose();
			oscP5 = null;
		}

		public void sendClearDrawingMessage() {

			OscMessage myMessage = new OscMessage("AndroidData");
			myMessage.add(CLEAR);

			bufferMessages.add(myMessage);
		}

		public void sendAnnotationMessage(Point p) {
			Point p2 = new Point(p);
			p2.scaleTo640x360();

			OscMessage myMessage = new OscMessage("AndroidData");
			myMessage.add(p2.a);
			myMessage.add(p2.b);
			myMessage.add(p2.x);
			myMessage.add(p2.y);
			myMessage.add(p2.o);
			myMessage.add(localAnnotation.ordinal());

			bufferMessages.add(myMessage);

			// println("sending...");
		}

		public void sendBufferMessages() {
			if (bufferMessages.size() > 0) {
				for (int i = 0; i < bufferMessages.size(); i++) {
					Object m = bufferMessages.get(i);
					if (m != null) {
						OscMessage myMessage = (OscMessage) m;
						if (myMessage != null) {
							oscP5.flush(myMessage, remoteLocation);
						}
					}
				}

				bufferMessages.clear();
			}

			if (System.currentTimeMillis() - lastOrientationUpdate > 500) {
				// send the current orientation to the other device
				OscMessage myMessage = new OscMessage("AndroidData");
				myMessage.add(orientation.x);
				myMessage.add(orientation.y);
				myMessage.add(orientation.z);
				// oscP5.send(myMessage, remoteLocation);
				oscP5.flush(myMessage, remoteLocation);

				// println("sending orientation x:" + orientation.x);

				lastOrientationUpdate = System.currentTimeMillis();
			}
		}

		public void oscEvent(OscMessage theOscMessage) {
			// println("receiving");

			if (theOscMessage.checkTypetag("fffffi")) {
				println(theOscMessage);

				Point p = new Point();

				p.a = theOscMessage.get(0).floatValue();
				p.b = theOscMessage.get(1).floatValue();
				p.x = theOscMessage.get(2).floatValue();
				p.y = theOscMessage.get(3).floatValue();
				p.o = theOscMessage.get(4).floatValue();
				int s = theOscMessage.get(5).intValue();

				println("s: " + s);

				ANNOTATION_TYPE remoteScenario = ANNOTATION_TYPE.values()[s];

				println("remoteScenario: " + remoteScenario);

				p.scaleToScreen();

				switch (remoteScenario) {
				case DRAWING:
					drawing.remotePoints.add(p);
					remoteAnnotation = ANNOTATION_TYPE.DRAWING;
					break;

				case POINTING:
					pointing.remotePointingPoint.set(p);
					remoteAnnotation = ANNOTATION_TYPE.POINTING;
					break;

				default:
					println("invalid scenario:" + remoteScenario);
				}

				p.print();
				// p.draw();
			} else if (theOscMessage.checkTypetag("fff")) {
				float x = theOscMessage.get(0).floatValue();
				float y = theOscMessage.get(1).floatValue();
				float z = theOscMessage.get(2).floatValue();

				// float offset = -2.5625;

				// offset = map(mouseX, 0, width, -10, 10);
				// println("offset: " + offset + " x: " + x);

				remoteOrientation.set(x, y, z);
				// println("x: " + x + " y: " + y + " z: " + z);
			} else if (theOscMessage.checkTypetag("s")) {
				String command = theOscMessage.get(0).stringValue();
				if (command.equals(CLEAR)) {
					drawing.clearRemoteDrawing();
				}
			}
		}
	}

	class Drawing {
		ArrayList points = new ArrayList();
		ArrayList remotePoints = new ArrayList();

		public void drawLocalDrawing() {
			// draw line
			stroke(activeColor);

			loop(points);
		}

		public void drawRemoteDrawing() {
			stroke(remoteColor);
			loop(remotePoints);
		}

		public void loop(ArrayList array) {
			for (int i = 0; i < array.size(); i++) {
				Object p = array.get(i);
				if (p != null) {
					Point dots = (Point) p;
					drawCylinderLine(dots);
				}
			}
		}

		public void clearLocalDrawing() {
			points.clear();
		}

		public void clearRemoteDrawing() {
			remotePoints.clear();
		}
	}

	class Point {
		float a, b, x, y, o;

		Point() {
		}

		Point(Point p) {
			set(p);
		}

		Point(float apos, float bpos, float xpos, float ypos, float orientation) {
			a = apos;
			b = bpos;
			x = xpos;
			y = ypos;
			o = orientation;
		}

		public void set(Point p) {
			a = p.a;
			b = p.b;
			x = p.x;
			y = p.y;
			o = p.o;
		}

		public void scale(float w0, float h0, float w1, float h1) {
			a = map(a, 0, w0, 0, w1);
			b = map(b, 0, h0, 0, h1);
			x = map(x, 0, w0, 0, w1);
			y = map(y, 0, h0, 0, h1);
		}

		public void scaleTo640x360() {
			scale(width, height, 640, 360);
		}

		public void scaleToScreen() {
			scale(640, 360, width, height);
		}

		public void print() {
			println("point - a:" + this.a + " b:" + this.b + " x:" + this.x
					+ " y:" + this.y + " o:" + this.o);
		}
	}

	class Pointing {
		Point pointingPoint = new Point();
		Point remotePointingPoint = new Point();

		// triangle parameters
		float offset = 50;
		float factor = 3;

		public void drawLocalPointing() {
			// draw finger point
			stroke(activeColor);
			drawFingerPoint(pointingPoint);
		}

		public void drawRemotePointing() {
			stroke(remoteColor);
			drawFingerPoint(remotePointingPoint);
		}

		public void drawFingerPoint(Point point) {
			pushMatrix();
			translate(0, -cheight / 2);

			strokeWeight(5);

			float angle = atan2((point.y - point.b), (point.x - point.a));

			PVector v1 = createVector(-offset * factor, -offset, point, angle);
			PVector v2 = createVector(0, 0, point, angle);
			PVector v3 = createVector(-offset * factor, +offset, point, angle);

			PVector c1 = getCylinderPoint(v1.x, v1.y, point.o);
			PVector c2 = getCylinderPoint(v2.x, v2.y, point.o);
			PVector c3 = getCylinderPoint(v3.x, v3.y, point.o);

			line(c1.x, c1.y, c1.z, c2.x, c2.y, c2.z);
			line(c1.x, c1.y, c1.z, c3.x, c3.y, c3.z);
			line(c3.x, c3.y, c3.z, c2.x, c2.y, c2.z);

			popMatrix();
		}

		public PVector createVector(float x0, float y0, Point point, float angle) {
			PVector v = new PVector(x0, y0);
			v.rotate(angle);
			v.add(point.x, point.y, 0);

			return v;
		}
	}

	class Radar {
		public void drawContextCompass() {
			pushMatrix();
			ortho();

			// draw centerpoint
			stroke(activeColor);
			strokeWeight(4);
			fill(255, 50);
			ellipse(0, 0, height / 8, height / 8);

			stroke(activeColor);
			line(-width / 3, -height / 3, width / 3, -height / 3);
			noStroke();
			translate(-width / 3, 0, 0);

			float ratio = (width / 3 * 2) / 360.0f;

			int rectWidth = 500;
			int rectHeight = 50;

			fill(activeColor, 150);
			ellipse(orientation.x * ratio, -height / 3, 10, rectHeight);
			fill(activeColor, 100);
			rect(orientation.x * ratio - rectWidth / 2, -height / 3
					- rectHeight / 2, rectWidth, rectHeight);

			fill(remoteColor, 150);
			ellipse(remoteOrientation.x * ratio, -height / 3, 10, rectHeight);
			fill(remoteColor, 100);
			rect(remoteOrientation.x * ratio - rectWidth / 2, -height / 3
					- rectHeight / 2, rectWidth, rectHeight);
			popMatrix();
		}

		public void drawCenteredRadar() {
			pushMatrix();
			ortho();

			// draw ellipses
			pushMatrix();
			// draw centerpoint
			stroke(activeColor);
			strokeWeight(4);
			fill(255, 50);
			ellipse(0, 0, height / 8, height / 8);

			popMatrix();

			// new active Radar middle point
			pushMatrix();
			noStroke();
			fill(activeColor, 150);
			rotateZ(radians(orientation.x));
			drawArc(450, 55);
			popMatrix();

			// new active Radar area
			pushMatrix();
			noStroke();
			fill(activeColor, 30);
			rotateZ(radians(orientation.x));
			drawArc(400, 3);
			popMatrix();

			// new active Radar stroke
			pushMatrix();
			stroke(activeColor);
			strokeWeight(5);
			rotateZ(radians(orientation.x));
			drawArc(400, 3);
			popMatrix();

			// new remote radar middle point
			pushMatrix();
			noStroke();
			fill(remoteColor, 150);
			rotateZ(radians(remoteOrientation.x));
			drawArc(450, 55);
			popMatrix();

			// new remote radar area
			pushMatrix();
			noStroke();
			fill(remoteColor, 50);
			rotateZ(radians(remoteOrientation.x));
			drawArc(400, 3);
			popMatrix();

			// new remote radar stroke
			pushMatrix();
			strokeWeight(5);
			stroke(remoteColor);
			noFill();
			rotateZ(radians(remoteOrientation.x));
			drawArc(400, 3);
			popMatrix();

			popMatrix();
		}

		public void drawArc(float arcSize, float arcStep) {
			startSize = arcSize;
			step = arcStep;
			startAngle = rotateAngle - PI / step;
			endAngle = rotateAngle + PI / step;
			arc(0, 0, startSize, startSize, startAngle, endAngle);
		}

		public void drawOldRadar() {
			pushMatrix();
			ortho();
			translate(width / 3, -height / 3);

			// white radar
			pushMatrix();

			// strokeWeight(5);
			noStroke();
			fill(activeColor, 150);

			rotateZ(radians(orientation.x));
			drawArc(450, 70);
			popMatrix();

			pushMatrix();
			// strokeWeight(5);
			noStroke();
			fill(activeColor, 70);
			rotateZ(radians(orientation.x));
			drawArc(400, 3);

			popMatrix();

			// turquoise radar

			pushMatrix();
			// strokeWeight(5);
			noStroke();
			fill(remoteColor, 150);
			rotateZ(radians(remoteOrientation.x));
			drawArc(450, 80);
			popMatrix();

			pushMatrix();
			// strokeWeight(5);
			noStroke();
			fill(remoteColor, 100);

			rotateZ(radians(remoteOrientation.x));
			drawArc(400, 3);
			popMatrix();

			// draw ellipses
			pushMatrix();
			noFill();
			stroke(255, 50);
			strokeWeight(4);
			ellipse(0, 0, height / 4, height / 4);
			strokeWeight(2);
			ellipse(0, 0, height / 5, height / 5);
			strokeWeight(1);
			ellipse(0, 0, height / 6, height / 6);
			ellipse(0, 0, height / 8, height / 8);
			ellipse(0, 0, height / 11, height / 11);

			fill(255);
			ellipse(0, 0, 5, 5);
			popMatrix();

			// draw centerpoint
			stroke(255);
			strokeWeight(4);
			fill(255, 50);
			ellipse(-width / 3, height / 3, height / 8, height / 8);

			popMatrix();
		}
	}

	class Rectangle {
		int remoteColor = color(100, 100, 255);
		float padding = 10;
		float imageScreenWidth;

		Rectangle(float imageScreenWidth) {
			this.imageScreenWidth = imageScreenWidth;
			// padding = map(mouseX, 0, width, 0, 100);
			// println("padding: " + padding);
		}

		public float calculateOffset(PVector orientation,
				PVector remoteOrientation) {
			float diff = orientation.x - remoteOrientation.x;
			diff = diff > 180 ? diff - 360 : diff;

			// println("orientation.x: " + orientation.x + " diff: " + diff+
			// " offsetX: " + offsetX);

			float offsetX = map(-diff, -180.0f, 180.0f, -imageScreenWidth / 2,
					imageScreenWidth / 2);

			return offsetX;
		}

		public void pre() {
			pushMatrix();
			ortho();
		}

		public void post() {
			popMatrix();
		}

		public void drawRectangle(PVector orientation, PVector remoteOrientation) {
			pre();

			drawLocalRectangle();

			float offsetX = calculateOffset(orientation, remoteOrientation);
			translate(offsetX, 0, 0);

			drawRectangle();

			post();
		}

		public void drawCenterPoint(PVector orientation,
				PVector remoteOrientation) {
			pre();

			drawLocalCenterPoint();

			float offsetX = calculateOffset(orientation, remoteOrientation);
			translate(offsetX, 0, 0);

			drawCenterPoint();

			post();
		}

		public void drawLocalRectangle() {
			// draw rectangle
			strokeWeight(10);
			stroke(activeColor, 150);
			noFill();
			rect(-width / 2 + padding, -height / 2 + padding, width - padding
					* 2, height - padding * 2);
		}

		public void drawRectangle() {
			// draw rectangle
			strokeWeight(10);
			stroke(remoteColor, 150);
			noFill();
			rect(-width / 2 + padding, -height / 2 + padding, width - padding
					* 2, height - padding * 2);
		}

		public void drawLocalCenterPoint() {
			// draw center point
			strokeWeight(1);
			fill(activeColor, 150);
			ellipse(0, 0, height / 8, height / 8);
		}

		public void drawCenterPoint() {
			// draw center point
			strokeWeight(1);
			fill(remoteColor, 150);
			ellipse(0, 0, height / 8, height / 8);
		}
	}

	class Status {
		PImage imgDrawing, imgPointing;
		int imgPadding = 10;

		Status() {
			imgDrawing = loadImage("icon-drawing.png");
			imgPointing = loadImage("icon-pointing.png");
		}

		public void drawStatus() {
			pushMatrix();
			ortho();

			translate(-width / 2, height / 2);
			textSize(15);
			fill(255, 0, 0);
			text("scenario: " + scenario, 0, 0);

			translate(0, -10);
			text("local: " + drawing.points.size(), 0, 0);

			translate(0, -10);
			text("remote: " + drawing.remotePoints.size(), 0, 0);

			popMatrix();

			drawAnnotationTypeIcon();
		}

		public void drawAnnotationTypeIcon() {
			pushMatrix();
			ortho();

			PImage img = getIconImage();

			// float x = map(mouseX, 0, width, 0, 500);
			// println("x: " + x);

			float x = 200; // offset
			float y = 20;

			translate(width / 2, height / 2);
			image(img, -img.width - imgPadding - x, -img.height - imgPadding
					- y);

			popMatrix();
		}

		public PImage getIconImage() {
			return localAnnotation == ANNOTATION_TYPE.DRAWING ? imgDrawing
					: imgPointing;
		}
	}

	// TEST COMMENT
	class File {

		PrintWriter output;
		long lastUpdate = 0;

		File() {
			int D = day(); // Values from 1 - 31
			int M = month(); // Values from 1 - 12
			int Y = year(); // 2003, 2004, 2005, etc.

			int ms = millis();
			int s = second(); // Values from 0 - 59
			int m = minute(); // Values from 0 - 59
			int h = hour(); // Values from 0 - 23

			String fileName = String
					.format("Experiment %s_%s_%s_%s_%s_%s_%s.csv", Y, M, D, h,
							m, s, ms);
			String basePath = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
			String directory = "/panorama_experiment/";
			String filePath = basePath + directory + fileName;

			output = createWriter(filePath);
		}

		public boolean isReady() {
			if (System.currentTimeMillis() - lastUpdate > 500) {
				lastUpdate = System.currentTimeMillis();
				return true;
			}
			return false;
		}

		public void writeTime() {
			int D = day(); // Values from 1 - 31
			int M = month(); // Values from 1 - 12
			int Y = year(); // 2003, 2004, 2005, etc.

			int s = second(); // Values from 0 - 59
			int m = minute(); // Values from 0 - 59
			int h = hour(); // Values from 0 - 23

			output.print(String.format("TABLET,%s-%s-%s %s:%s:%s", Y, M, D, h,
					m, s));
		}

		public void write(String text) {
			writeTime();
			output.print(String.format(",%s", text));
			output.println();
		}

		public void writeVector(PVector vector, String name) {
			writeTime();
			output.print(String.format(",%s,%s,%s,%s", name, vector.x,
					vector.y, vector.z));
		}

		public void writeOrientation(PVector local, PVector remote) {
			if (isReady()) {
				writeVector(local, "local");
				writeVector(remote, "remote");
				output.println();
			}
		}

		public void writeAnnotation(ANNOTATION_TYPE annotation, Point point) {
			writeTime();
			output.print(String.format(",%s,%s,%s,%s,%s,%s", annotation,
					point.a, point.b, point.x, point.y, point.o));
			output.println();
			output.flush();
		}

		public void writeClearAnnotation() {
			writeTime();
			output.print(String.format(",CLEAR"));
			output.println();
			output.flush();
		}

		public void destroy() {
			output.flush(); // Writes the remaining data to the file
			output.close(); // Finishes the file
		}
	}

	public int sketchWidth() {
		return displayWidth;
	}

	public int sketchHeight() {
		return displayHeight;
	}

	public String sketchRenderer() {
		return P3D;
	}
}
