import java.io.*;
import java.io.File;
import java.lang.StringBuilder;
import java.lang.Thread;
import android.os.Handler;
import java.io.FileWriter;
import java.io.BufferedWriter;
import android.os.Environment;
import java.util.Date;
import java.text.*;


// TEST COMMENT
class File {  

  PrintWriter output;
  long lastUpdate = 0;
  int pauseInitial = 2000; 
  int pausePerSecond = 500; 
  Thread thread;

  File()
  {
    int D = day();    // Values from 1 - 31
    int M = month();  // Values from 1 - 12
    int Y = year();   // 2003, 2004, 2005, etc.

    int ms = millis();
    int s = second();  // Values from 0 - 59
    int m = minute();  // Values from 0 - 59
    int h = hour();    // Values from 0 - 23

    String fileName = String.format("Experiment %s_%s_%s_%s_%s_%s_%s.csv", Y, M, D, h, m, s, ms);    
    String basePath = Environment.getExternalStorageDirectory().getAbsolutePath();
    String directory = "/panorama_experiment/";
    String filePath = basePath + directory+ fileName; 

    output = createWriter(filePath);
  }

  void readFileAsync() {     
    thread = new Thread() {
      public void run() {
        readFile();
      }
    };

    thread.start();
  }

  void testDate() {
    Date d = null;
    try {
      d = new SimpleDateFormat("yyyy-M-d H:m:s").parse("2014-2-20 18:34:3");
    }
    catch(Exception e) {
      println("error parsing!!!");
    }

    println("is null: " + (d==null));
    println(d);
  }

  DateFormat df = new SimpleDateFormat("yyyy-M-d H:m:s");
  DateFormat tf = new SimpleDateFormat("HH:mm:ss");

  Date getTime(String line) {
    //    println("getTime line: " + line);

    try {
      String [] tokens = line.split(",");
      //      println("tokens length: " + tokens.length);

      String time = tokens[1];
      //      println("time: " + time);

      Date date =  df.parse(time);
      //      println(date);

      return date;
    }
    catch(Exception e) {
      println(String.format("Error parsing [%s]: %s", line, e));
      return null;
    }
  }

  Date compareToLastDate(Date date, Date lastDate) throws InterruptedException {
    if (!date.equals(lastDate)) {
      println(String.format("sleeping... date: %s last: %s", date, lastDate));
      Thread.sleep(pausePerSecond);

      lastDate = date;
    }
    
    return lastDate;
  }










  
  String startTime = "18:38";

  void readFile() {
    Boolean skip1 = true, skip2 = true;
    java.io.File sdcard = Environment.getExternalStorageDirectory();

    //Get the text file
    java.io.File file = new java.io.File(sdcard, "2logs/glass_raw/Experiment_2014220_183554_1160.csv");
    java.io.File file2 = new java.io.File(sdcard, "2logs/tablet_raw/Experiment 2014_2_20_18_34_0_1093.csv");


    println("readFile start...");
    try {
      Thread.sleep(pauseInitial);

      BufferedReader br = new BufferedReader(new FileReader(file));
      BufferedReader br2 = new BufferedReader(new FileReader(file2));
      String line = br.readLine(), line2 = br2.readLine();
      Date lastDate1 = new Date(), lastDate2 = new Date();
      Boolean nextLine = false;

      while ( line != null || line2 != null ) { 
        Date date1 = getTime(line);      
        Date date2 = getTime(line2);
                  
        if (date1 == null) {
          line = br.readLine();          
        }
        
        if (date2 == null) {
          line2 = br2.readLine();          
        }
        
        if ((date1 == null && line != null) || 
            (date2 == null && line2 != null)){
          continue;
        }
        
        
        if(date1 == null) date1 = new Date();
        if(date2 == null) date2 = new Date();
        
        
        if(skip1 && !date1.toString().contains(startTime)){
          println(String.format("skipping... %s", line));
          line = br.readLine();
          continue;          
        }
        else{
          skip1 = false;
        }
        
        if(skip2 && !date2.toString().contains(startTime)){
          println(String.format("skipping... %s", line2));
          line2 = br2.readLine();
          continue;          
        }
        else{
          skip2 = false;
        }
          
        
        int compare = date1.compareTo(date2);

        println(String.format("date1: %s date2: %s compare: %s", date1, date2, compare));

        if (date1.before(date2)) {
          lastDate1 = compareToLastDate(date1, lastDate1);

          processGlassFile(line);
          line = br.readLine();
        }
        else if (date1.after(date2)) {
          lastDate2 = compareToLastDate(date2, lastDate2);

          processTabletFile(line2);
          line2 = br2.readLine();
        }
        else if (date1.equals(date2)) {       
          lastDate1 = compareToLastDate(date1, lastDate1);
          lastDate2 = date2;

          processGlassFile(line);
          processTabletFile(line2);
          line = br.readLine();
          line2 = br2.readLine();
        }
      }
    }
    catch (IOException e) {
      //You'll need to add proper error handling here
      println("ERROR! readFile:IOException " + e.getMessage());
    }
    catch(InterruptedException e) { 
      println("ERROR! readFile:InterruptedException " + e.getMessage());
    }

    println("readFile success!");
  }

  void processGlassFile(String line) {
    println(line);

    String [] tokens = line.split(",");

    String time = tokens[1];
    String type = tokens[2];

    if (type.equals("local")) {
      println("local");

      remoteOrientation.set(float(tokens[3]), 0, 0);
      orientation.set(float(tokens[8]), 0, 0);
    } 
    else if (type.equals("POINTING")) {
      println("POINTING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));
      p.scaleToScreen();

      pointing.remotePointingPoint.set(p);  
    } 
    else if (type.equals("DRAWING")) {
      println("DRAWING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));

      p.scaleToScreen();

      drawing.remotePoints.add(p);
    }
    else if (type.equals("CLEAR")) {
      println("CLEAR");

      drawing.remotePoints.clear();
    }
  }

  void processTabletFile(String line) {
    println(line);
    String [] tokens = line.split(",");

    String time = tokens[1];
    String type = tokens[2];

    if (type.equals("local")) {
      println("local");

      orientation.set(float(tokens[3]), 0, 0);
      remoteOrientation.set(float(tokens[8]), 0, 0);
    } 
    else if (type.equals("POINTING")) {
      println("POINTING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));

      pointing.pointingPoint.set(p);
    } 
    else if (type.equals("DRAWING")) {
      println("DRAWING");

      Point p = new Point(
      float(tokens[3]), 
      float(tokens[4]), 
      float(tokens[5]), 
      float(tokens[6]), 
      float(tokens[7]));

      drawing.points.add(p);
    }
    else if (type.equals("CLEAR")) {
      println("CLEAR");

      drawing.points.clear();
    }
  }

  boolean isReady() 
  {
    if (System.currentTimeMillis() - lastUpdate > 500) {
      lastUpdate = System.currentTimeMillis();
      return true;
    }
    return false;
  }

  void writeTime()
  {
    int D = day();    // Values from 1 - 31
    int M = month();  // Values from 1 - 12
    int Y = year();   // 2003, 2004, 2005, etc.

    int s = second();  // Values from 0 - 59
    int m = minute();  // Values from 0 - 59
    int h = hour();    // Values from 0 - 23

    output.print(String.format("TABLET,%s-%s-%s %s:%s:%s", Y, M, D, h, m, s));
  }

  void write(String text)
  {
    writeTime();
    output.print(String.format(",%s", text));
    output.println();
  }

  void writeVector(PVector vector, String name)
  {
    writeTime();
    output.print(String.format(",%s,%s,%s,%s", name, vector.x, vector.y, vector.z));
  }

  void writeOrientation(PVector local, PVector remote)
  {
    if (isReady()) {
      writeVector(local, "local");
      writeVector(remote, "remote");
      output.println();
    }
  }    

  void writeAnnotation(ANNOTATION_TYPE annotation, Point point) {
    writeTime();
    output.print(String.format(",%s,%s,%s,%s,%s,%s", annotation, point.a, point.b, point.x, point.y, point.o));
    output.println();
    output.flush();
  }

  void writeClearAnnotation() {
    writeTime();
    output.print(String.format(",CLEAR"));
    output.println();
    output.flush();
  }

  void destroy()
  {
    output.flush(); // Writes the remaining data to the file
    output.close(); // Finishes the file
  }
}

