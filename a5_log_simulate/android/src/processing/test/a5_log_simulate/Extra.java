package processing.test.a5_log_simulate;

import android.app.Activity;
import android.os.Bundle;

public class Extra {
	public final static String EXTRA_OPTIONS = "EXTRA_OPTIONS";
	public final static String EXTRA_SIMULATE = "EXTRA_SIMULATE";
	
	public static boolean check(Activity activitiy, Object param){

		Bundle extras = activitiy.getIntent().getExtras();
		if (extras != null) {
			return extras.containsKey(param.toString());		    
		}
		
		return false;
	}
}
